﻿namespace Pacotes
{
    partial class FormApagarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonCancelar = new System.Windows.Forms.Button();
            this.ButtonApagar = new System.Windows.Forms.Button();
            this.ComboBoxApagarPacotes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // ButtonCancelar
            // 
            this.ButtonCancelar.BackColor = System.Drawing.Color.White;
            this.ButtonCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonCancelar.Image = global::Pacotes.Properties.Resources.ic_cancel;
            this.ButtonCancelar.Location = new System.Drawing.Point(467, 71);
            this.ButtonCancelar.Name = "ButtonCancelar";
            this.ButtonCancelar.Size = new System.Drawing.Size(61, 38);
            this.ButtonCancelar.TabIndex = 3;
            this.ButtonCancelar.UseVisualStyleBackColor = false;
            this.ButtonCancelar.Click += new System.EventHandler(this.ButtonCancelar_Click);
            // 
            // ButtonApagar
            // 
            this.ButtonApagar.BackColor = System.Drawing.Color.White;
            this.ButtonApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonApagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonApagar.Image = global::Pacotes.Properties.Resources.ic_ok;
            this.ButtonApagar.Location = new System.Drawing.Point(400, 71);
            this.ButtonApagar.Name = "ButtonApagar";
            this.ButtonApagar.Size = new System.Drawing.Size(61, 38);
            this.ButtonApagar.TabIndex = 2;
            this.ButtonApagar.UseVisualStyleBackColor = false;
            this.ButtonApagar.Click += new System.EventHandler(this.ButtonApagar_Click);
            // 
            // ComboBoxApagarPacotes
            // 
            this.ComboBoxApagarPacotes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ComboBoxApagarPacotes.FormattingEnabled = true;
            this.ComboBoxApagarPacotes.Location = new System.Drawing.Point(12, 26);
            this.ComboBoxApagarPacotes.Name = "ComboBoxApagarPacotes";
            this.ComboBoxApagarPacotes.Size = new System.Drawing.Size(515, 21);
            this.ComboBoxApagarPacotes.TabIndex = 4;
            // 
            // FormApagarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(552, 146);
            this.ControlBox = false;
            this.Controls.Add(this.ComboBoxApagarPacotes);
            this.Controls.Add(this.ButtonCancelar);
            this.Controls.Add(this.ButtonApagar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormApagarPacote";
            this.Text = "Apagar Viagens";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonCancelar;
        private System.Windows.Forms.Button ButtonApagar;
        private System.Windows.Forms.ComboBox ComboBoxApagarPacotes;
    }
}