﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacotes.Modelos
{
    public class Packs2
    {

        public int IdPacote { get; set; }
        public string DescricaoPacote { get; set; }
        public double Precopacote { get; set; }

        public override string ToString()
        {
            return string.Format("Id da Viagem: {0}  |  Descrição da Viagem: {1}  |  Preço: {2}€", IdPacote, DescricaoPacote, Precopacote);
        }


    }


}
