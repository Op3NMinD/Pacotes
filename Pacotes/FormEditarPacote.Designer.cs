﻿namespace Pacotes
{
    partial class FormEditarPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.TextBoxDescricaoPacote = new System.Windows.Forms.RichTextBox();
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.ButtonDireitaDireita = new System.Windows.Forms.Button();
            this.ButtonDireita = new System.Windows.Forms.Button();
            this.ButtonEsquerda = new System.Windows.Forms.Button();
            this.ButtonEsquerdaEsquerda = new System.Windows.Forms.Button();
            this.ButtonNovoPacoteCancelar = new System.Windows.Forms.Button();
            this.ButtonNovoPacoteAplicar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(108, 232);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 16);
            this.label4.TabIndex = 17;
            this.label4.Text = "€";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 16;
            this.label3.Text = "Preço";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 16);
            this.label2.TabIndex = 15;
            this.label2.Text = "Descrição da Viagem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "ID Viagem";
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(29, 231);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(73, 20);
            this.TextBoxPreco.TabIndex = 13;
            // 
            // TextBoxDescricaoPacote
            // 
            this.TextBoxDescricaoPacote.Location = new System.Drawing.Point(29, 86);
            this.TextBoxDescricaoPacote.Name = "TextBoxDescricaoPacote";
            this.TextBoxDescricaoPacote.Size = new System.Drawing.Size(288, 105);
            this.TextBoxDescricaoPacote.TabIndex = 12;
            this.TextBoxDescricaoPacote.Text = "";
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Enabled = false;
            this.TextBoxIdPacote.Location = new System.Drawing.Point(116, 22);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.Size = new System.Drawing.Size(66, 20);
            this.TextBoxIdPacote.TabIndex = 11;
            // 
            // ButtonDireitaDireita
            // 
            this.ButtonDireitaDireita.BackgroundImage = global::Pacotes.Properties.Resources.Ic_rightright;
            this.ButtonDireitaDireita.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonDireitaDireita.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDireitaDireita.Location = new System.Drawing.Point(272, 225);
            this.ButtonDireitaDireita.Name = "ButtonDireitaDireita";
            this.ButtonDireitaDireita.Size = new System.Drawing.Size(34, 30);
            this.ButtonDireitaDireita.TabIndex = 21;
            this.ButtonDireitaDireita.UseVisualStyleBackColor = true;
            this.ButtonDireitaDireita.Click += new System.EventHandler(this.ButtonDireitaDireita_Click);
            // 
            // ButtonDireita
            // 
            this.ButtonDireita.BackgroundImage = global::Pacotes.Properties.Resources.Ic_right;
            this.ButtonDireita.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonDireita.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonDireita.Location = new System.Drawing.Point(240, 225);
            this.ButtonDireita.Name = "ButtonDireita";
            this.ButtonDireita.Size = new System.Drawing.Size(26, 30);
            this.ButtonDireita.TabIndex = 20;
            this.ButtonDireita.UseVisualStyleBackColor = true;
            this.ButtonDireita.Click += new System.EventHandler(this.ButtonDireita_Click);
            // 
            // ButtonEsquerda
            // 
            this.ButtonEsquerda.BackgroundImage = global::Pacotes.Properties.Resources.Ic_left;
            this.ButtonEsquerda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonEsquerda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonEsquerda.Location = new System.Drawing.Point(208, 225);
            this.ButtonEsquerda.Name = "ButtonEsquerda";
            this.ButtonEsquerda.Size = new System.Drawing.Size(26, 30);
            this.ButtonEsquerda.TabIndex = 19;
            this.ButtonEsquerda.UseVisualStyleBackColor = true;
            this.ButtonEsquerda.Click += new System.EventHandler(this.ButtonEsquerda_Click);
            // 
            // ButtonEsquerdaEsquerda
            // 
            this.ButtonEsquerdaEsquerda.BackgroundImage = global::Pacotes.Properties.Resources.Ic_leftleft;
            this.ButtonEsquerdaEsquerda.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ButtonEsquerdaEsquerda.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonEsquerdaEsquerda.Location = new System.Drawing.Point(168, 225);
            this.ButtonEsquerdaEsquerda.Name = "ButtonEsquerdaEsquerda";
            this.ButtonEsquerdaEsquerda.Size = new System.Drawing.Size(34, 30);
            this.ButtonEsquerdaEsquerda.TabIndex = 18;
            this.ButtonEsquerdaEsquerda.UseVisualStyleBackColor = true;
            this.ButtonEsquerdaEsquerda.Click += new System.EventHandler(this.ButtonEsquerdaEsquerda_Click);
            // 
            // ButtonNovoPacoteCancelar
            // 
            this.ButtonNovoPacoteCancelar.BackColor = System.Drawing.Color.White;
            this.ButtonNovoPacoteCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonNovoPacoteCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonNovoPacoteCancelar.Image = global::Pacotes.Properties.Resources.ic_cancel;
            this.ButtonNovoPacoteCancelar.Location = new System.Drawing.Point(245, 276);
            this.ButtonNovoPacoteCancelar.Name = "ButtonNovoPacoteCancelar";
            this.ButtonNovoPacoteCancelar.Size = new System.Drawing.Size(61, 36);
            this.ButtonNovoPacoteCancelar.TabIndex = 10;
            this.ButtonNovoPacoteCancelar.UseVisualStyleBackColor = false;
            this.ButtonNovoPacoteCancelar.Click += new System.EventHandler(this.ButtonNovoPacoteCancelar_Click);
            // 
            // ButtonNovoPacoteAplicar
            // 
            this.ButtonNovoPacoteAplicar.BackColor = System.Drawing.Color.White;
            this.ButtonNovoPacoteAplicar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonNovoPacoteAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonNovoPacoteAplicar.Image = global::Pacotes.Properties.Resources.ic_ok;
            this.ButtonNovoPacoteAplicar.Location = new System.Drawing.Point(178, 276);
            this.ButtonNovoPacoteAplicar.Name = "ButtonNovoPacoteAplicar";
            this.ButtonNovoPacoteAplicar.Size = new System.Drawing.Size(61, 36);
            this.ButtonNovoPacoteAplicar.TabIndex = 9;
            this.ButtonNovoPacoteAplicar.UseVisualStyleBackColor = false;
            this.ButtonNovoPacoteAplicar.Click += new System.EventHandler(this.ButtonNovoPacoteAplicar_Click);
            // 
            // FormEditarPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 332);
            this.ControlBox = false;
            this.Controls.Add(this.ButtonDireitaDireita);
            this.Controls.Add(this.ButtonDireita);
            this.Controls.Add(this.ButtonEsquerda);
            this.Controls.Add(this.ButtonEsquerdaEsquerda);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricaoPacote);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.ButtonNovoPacoteCancelar);
            this.Controls.Add(this.ButtonNovoPacoteAplicar);
            this.Name = "FormEditarPacote";
            this.Text = "Editar Viagens";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.RichTextBox TextBoxDescricaoPacote;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.Button ButtonNovoPacoteCancelar;
        private System.Windows.Forms.Button ButtonNovoPacoteAplicar;
        private System.Windows.Forms.Button ButtonEsquerdaEsquerda;
        private System.Windows.Forms.Button ButtonEsquerda;
        private System.Windows.Forms.Button ButtonDireita;
        private System.Windows.Forms.Button ButtonDireitaDireita;
    }
}