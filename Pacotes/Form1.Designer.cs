﻿namespace Pacotes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PictureBoxLer = new System.Windows.Forms.PictureBox();
            this.PictureBoxAdicionar = new System.Windows.Forms.PictureBox();
            this.PictureBoxEdit = new System.Windows.Forms.PictureBox();
            this.PictureBoxApagar = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PictureBoxFechar = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.configuraçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carregarListaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gravarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gravarComoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAdicionar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxApagar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFechar)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuraçõesToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(576, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.PictureBoxLer);
            this.panel1.Controls.Add(this.PictureBoxAdicionar);
            this.panel1.Controls.Add(this.PictureBoxEdit);
            this.panel1.Controls.Add(this.PictureBoxApagar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(576, 130);
            this.panel1.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(474, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 18);
            this.label7.TabIndex = 17;
            this.label7.Text = "Viagens";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(470, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 18);
            this.label8.TabIndex = 16;
            this.label8.Text = "Cancelar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(331, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 18);
            this.label5.TabIndex = 15;
            this.label5.Text = "Dados";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(328, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "Alterar";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(179, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 18);
            this.label3.TabIndex = 13;
            this.label3.Text = "Viagens";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(175, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 18);
            this.label4.TabIndex = 12;
            this.label4.Text = "Pesquisar";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(45, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Viagem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gill Sans MT", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 18);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nova";
            // 
            // PictureBoxLer
            // 
            this.PictureBoxLer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxLer.Image = global::Pacotes.Properties.Resources.zoom;
            this.PictureBoxLer.Location = new System.Drawing.Point(182, 60);
            this.PictureBoxLer.Name = "PictureBoxLer";
            this.PictureBoxLer.Size = new System.Drawing.Size(48, 50);
            this.PictureBoxLer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxLer.TabIndex = 8;
            this.PictureBoxLer.TabStop = false;
            this.toolTip2.SetToolTip(this.PictureBoxLer, "Pesquisar Viagens");
            this.PictureBoxLer.Click += new System.EventHandler(this.PictureBoxLer_Click);
            this.PictureBoxLer.MouseLeave += new System.EventHandler(this.PictureBoxLer_MouseLeave);
            this.PictureBoxLer.MouseHover += new System.EventHandler(this.PictureBoxLer_MouseHover);
            // 
            // PictureBoxAdicionar
            // 
            this.PictureBoxAdicionar.BackgroundImage = global::Pacotes.Properties.Resources.add;
            this.PictureBoxAdicionar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PictureBoxAdicionar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxAdicionar.Image = global::Pacotes.Properties.Resources.add;
            this.PictureBoxAdicionar.Location = new System.Drawing.Point(48, 60);
            this.PictureBoxAdicionar.Name = "PictureBoxAdicionar";
            this.PictureBoxAdicionar.Size = new System.Drawing.Size(48, 50);
            this.PictureBoxAdicionar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxAdicionar.TabIndex = 6;
            this.PictureBoxAdicionar.TabStop = false;
            this.toolTip1.SetToolTip(this.PictureBoxAdicionar, "Nova Viagem");
            this.PictureBoxAdicionar.Click += new System.EventHandler(this.PictureBoxAdicionar_Click);
            this.PictureBoxAdicionar.MouseLeave += new System.EventHandler(this.PictureBoxAdicionar_MouseLeave);
            this.PictureBoxAdicionar.MouseHover += new System.EventHandler(this.PictureBoxAdicionar_MouseHover);
            // 
            // PictureBoxEdit
            // 
            this.PictureBoxEdit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxEdit.Image = global::Pacotes.Properties.Resources.refresh;
            this.PictureBoxEdit.Location = new System.Drawing.Point(330, 60);
            this.PictureBoxEdit.Name = "PictureBoxEdit";
            this.PictureBoxEdit.Size = new System.Drawing.Size(48, 50);
            this.PictureBoxEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxEdit.TabIndex = 9;
            this.PictureBoxEdit.TabStop = false;
            this.toolTip3.SetToolTip(this.PictureBoxEdit, "Alterar Dados");
            this.PictureBoxEdit.Click += new System.EventHandler(this.PictureBoxEdit_Click);
            this.PictureBoxEdit.MouseLeave += new System.EventHandler(this.PictureBoxEdit_MouseLeave);
            this.PictureBoxEdit.MouseHover += new System.EventHandler(this.PictureBoxEdit_MouseHover);
            // 
            // PictureBoxApagar
            // 
            this.PictureBoxApagar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxApagar.Image = global::Pacotes.Properties.Resources.Recycle;
            this.PictureBoxApagar.Location = new System.Drawing.Point(477, 60);
            this.PictureBoxApagar.Name = "PictureBoxApagar";
            this.PictureBoxApagar.Size = new System.Drawing.Size(48, 50);
            this.PictureBoxApagar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxApagar.TabIndex = 7;
            this.PictureBoxApagar.TabStop = false;
            this.toolTip4.SetToolTip(this.PictureBoxApagar, "Cancelar Viagens");
            this.PictureBoxApagar.Click += new System.EventHandler(this.PictureBoxApagar_Click);
            this.PictureBoxApagar.MouseLeave += new System.EventHandler(this.PictureBoxApagar_MouseLeave);
            this.PictureBoxApagar.MouseHover += new System.EventHandler(this.PictureBoxApagar_MouseHover);
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::Pacotes.Properties.Resources.p018hpg1;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 152);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(576, 250);
            this.panel2.TabIndex = 12;
            // 
            // PictureBoxFechar
            // 
            this.PictureBoxFechar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxFechar.Image = global::Pacotes.Properties.Resources.close;
            this.PictureBoxFechar.Location = new System.Drawing.Point(552, 0);
            this.PictureBoxFechar.Name = "PictureBoxFechar";
            this.PictureBoxFechar.Size = new System.Drawing.Size(24, 24);
            this.PictureBoxFechar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxFechar.TabIndex = 10;
            this.PictureBoxFechar.TabStop = false;
            this.PictureBoxFechar.Click += new System.EventHandler(this.PictureBoxFechar_Click);
            this.PictureBoxFechar.MouseLeave += new System.EventHandler(this.PictureBoxFechar_MouseLeave);
            this.PictureBoxFechar.MouseHover += new System.EventHandler(this.PictureBoxFechar_MouseHover);
            // 
            // configuraçõesToolStripMenuItem
            // 
            this.configuraçõesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.carregarListaToolStripMenuItem,
            this.gravarToolStripMenuItem,
            this.gravarComoToolStripMenuItem});
            this.configuraçõesToolStripMenuItem.Name = "configuraçõesToolStripMenuItem";
            this.configuraçõesToolStripMenuItem.Size = new System.Drawing.Size(96, 20);
            this.configuraçõesToolStripMenuItem.Text = "Configurações";
            // 
            // carregarListaToolStripMenuItem
            // 
            this.carregarListaToolStripMenuItem.Name = "carregarListaToolStripMenuItem";
            this.carregarListaToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.carregarListaToolStripMenuItem.Text = "Carregar Lista";
            this.carregarListaToolStripMenuItem.Click += new System.EventHandler(this.carregarListaToolStripMenuItem_Click);
            // 
            // gravarToolStripMenuItem
            // 
            this.gravarToolStripMenuItem.Name = "gravarToolStripMenuItem";
            this.gravarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.gravarToolStripMenuItem.Text = "Gravar ";
            this.gravarToolStripMenuItem.Click += new System.EventHandler(this.gravarToolStripMenuItem_Click);
            // 
            // gravarComoToolStripMenuItem
            // 
            this.gravarComoToolStripMenuItem.Name = "gravarComoToolStripMenuItem";
            this.gravarComoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.gravarComoToolStripMenuItem.Text = "Gravar como...";
            this.gravarComoToolStripMenuItem.Click += new System.EventHandler(this.gravarComoToolStripMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(576, 402);
            this.ControlBox = false;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PictureBoxFechar);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Menu Principal";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxLer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxAdicionar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxApagar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxFechar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.PictureBox PictureBoxAdicionar;
        private System.Windows.Forms.PictureBox PictureBoxApagar;
        private System.Windows.Forms.PictureBox PictureBoxLer;
        private System.Windows.Forms.PictureBox PictureBoxEdit;
        private System.Windows.Forms.PictureBox PictureBoxFechar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ToolTip toolTip2;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolStripMenuItem configuraçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem carregarListaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gravarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gravarComoToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

