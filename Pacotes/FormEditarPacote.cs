﻿namespace Pacotes
{
    using LivrariaDeClasses;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    public partial class FormEditarPacote : Form
    {
        new Form1 Menu;
        int index = 0;
        List<Packs> ListaDePacotes = ListaDePacks.LoadPacotes();

        public FormEditarPacote(Form1 menu)
        {
            InitializeComponent();
            this.Menu = menu;
            if (ListaDePacotes != null)
            {
                MostraDados(index);
            }

        }

        private void MostraDados(int index)
        {
            TextBoxIdPacote.Text = ListaDePacotes[index].IdPacote.ToString();
            TextBoxDescricaoPacote.Text = ListaDePacotes[index].DescricaoPacote.ToString();
            TextBoxPreco.Text = ListaDePacotes[index].Precopacote.ToString();
        }

        private void ButtonNovoPacoteAplicar_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes[index].DescricaoPacote == TextBoxDescricaoPacote.Text && ListaDePacotes[index].Precopacote == Convert.ToDouble(TextBoxPreco.Text))
            {
                MessageBox.Show("Não fez alteração nenhuma!");
            }
            else
            {
                ListaDePacotes[index].DescricaoPacote = TextBoxDescricaoPacote.Text;
                ListaDePacotes[index].Precopacote = Convert.ToDouble(TextBoxPreco.Text);
                MessageBox.Show("Viagem Alterada com Sucesso");
                Close();
                Menu.Show();
            }

        }

        private void ButtonNovoPacoteCancelar_Click(object sender, EventArgs e)
        {
            Close();
            Menu.Show();
        }

        private void ButtonDireita_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                index++;

                if (index > (ListaDePacotes.Count - 1))
                {
                    index = 0;
                }

                MostraDados(index);

            }
        }

        private void ButtonEsquerda_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                index--;
                if (index < 0)
                {
                    index = ListaDePacotes.Count - 1;
                }

                MostraDados(index);
            }
        }


        private void ButtonDireitaDireita_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                index = ListaDePacotes.Count - 1;
                MostraDados(index);
            }
        }

        private void ButtonEsquerdaEsquerda_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                index = 0;
                MostraDados(index);
            }
        }
    }
}
