﻿namespace Pacotes
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using LivrariaDeClasses;
    using System.Linq;

    public partial class FormListarPacotes : Form
    {
        new Form1 Menu;

        List<Packs> ListaDePacotes = ListaDePacks.LoadPacotes();

        public FormListarPacotes(Form1 menu)
        {
            
            InitializeComponent();
            this.Menu = menu;
            DataGridViewListar.DataSource = ListaDePacotes;


            if (ListaDePacotes != null)
            {
                DataGridViewListar.Columns[0].HeaderText = "ID";
                DataGridViewListar.Columns[1].HeaderText = "Descrição da Viagem";
                DataGridViewListar.Columns[2].HeaderText = "Preço";
                DataGridViewListar.Columns[0].FillWeight = 15;
                DataGridViewListar.Columns[1].FillWeight = 120;
                DataGridViewListar.Columns[2].FillWeight = 30;
            }
        }



        private void button1_Click(object sender, System.EventArgs e)
        {
            Close();
            Menu.Show();
        }

        private void DataGridViewListar_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {          

            switch (e.ColumnIndex)
            {
                case 0:
                    {
                        DataGridViewListar.DataSource = ListaDePacotes.OrderBy(x => x.IdPacote).ToList();
                        break;
                    }
                case 1:
                    {
                        DataGridViewListar.DataSource = ListaDePacotes.OrderBy(x => x.DescricaoPacote).ToList();
                        break;
                    }
                case 2:
                    {
  
                        DataGridViewListar.DataSource = ListaDePacotes.OrderBy(x => x.Precopacote).ToList();
                        break;
                    }

                default:
                    break;
            }
                     
        }

    }
}
