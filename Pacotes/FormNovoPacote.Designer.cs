﻿namespace Pacotes
{
    partial class FormNovoPacote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxIdPacote = new System.Windows.Forms.TextBox();
            this.TextBoxDescricaoPacote = new System.Windows.Forms.RichTextBox();
            this.TextBoxPreco = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ButtonNovoPacoteCancelar = new System.Windows.Forms.Button();
            this.ButtonNovoPacoteAplicar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TextBoxIdPacote
            // 
            this.TextBoxIdPacote.Enabled = false;
            this.TextBoxIdPacote.Location = new System.Drawing.Point(124, 20);
            this.TextBoxIdPacote.Name = "TextBoxIdPacote";
            this.TextBoxIdPacote.Size = new System.Drawing.Size(71, 20);
            this.TextBoxIdPacote.TabIndex = 2;
            // 
            // TextBoxDescricaoPacote
            // 
            this.TextBoxDescricaoPacote.Location = new System.Drawing.Point(45, 86);
            this.TextBoxDescricaoPacote.Name = "TextBoxDescricaoPacote";
            this.TextBoxDescricaoPacote.Size = new System.Drawing.Size(315, 146);
            this.TextBoxDescricaoPacote.TabIndex = 3;
            this.TextBoxDescricaoPacote.Text = "";
            // 
            // TextBoxPreco
            // 
            this.TextBoxPreco.Location = new System.Drawing.Point(45, 264);
            this.TextBoxPreco.Name = "TextBoxPreco";
            this.TextBoxPreco.Size = new System.Drawing.Size(92, 20);
            this.TextBoxPreco.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "ID Viagem";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(42, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "Descrição da Viagem";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(42, 244);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Preço";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(143, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(16, 16);
            this.label4.TabIndex = 8;
            this.label4.Text = "€";
            // 
            // ButtonNovoPacoteCancelar
            // 
            this.ButtonNovoPacoteCancelar.BackColor = System.Drawing.Color.White;
            this.ButtonNovoPacoteCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonNovoPacoteCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonNovoPacoteCancelar.Image = global::Pacotes.Properties.Resources.ic_cancel;
            this.ButtonNovoPacoteCancelar.Location = new System.Drawing.Point(299, 255);
            this.ButtonNovoPacoteCancelar.Name = "ButtonNovoPacoteCancelar";
            this.ButtonNovoPacoteCancelar.Size = new System.Drawing.Size(61, 37);
            this.ButtonNovoPacoteCancelar.TabIndex = 1;
            this.ButtonNovoPacoteCancelar.UseVisualStyleBackColor = false;
            this.ButtonNovoPacoteCancelar.Click += new System.EventHandler(this.ButtonNovoPacoteCancelar_Click);
            // 
            // ButtonNovoPacoteAplicar
            // 
            this.ButtonNovoPacoteAplicar.BackColor = System.Drawing.Color.White;
            this.ButtonNovoPacoteAplicar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonNovoPacoteAplicar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.ButtonNovoPacoteAplicar.Image = global::Pacotes.Properties.Resources.ic_ok;
            this.ButtonNovoPacoteAplicar.Location = new System.Drawing.Point(232, 255);
            this.ButtonNovoPacoteAplicar.Name = "ButtonNovoPacoteAplicar";
            this.ButtonNovoPacoteAplicar.Size = new System.Drawing.Size(61, 37);
            this.ButtonNovoPacoteAplicar.TabIndex = 0;
            this.ButtonNovoPacoteAplicar.UseVisualStyleBackColor = false;
            this.ButtonNovoPacoteAplicar.Click += new System.EventHandler(this.ButtonNovoPacoteAplicar_Click);
            // 
            // FormNovoPacote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(405, 316);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TextBoxPreco);
            this.Controls.Add(this.TextBoxDescricaoPacote);
            this.Controls.Add(this.TextBoxIdPacote);
            this.Controls.Add(this.ButtonNovoPacoteCancelar);
            this.Controls.Add(this.ButtonNovoPacoteAplicar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormNovoPacote";
            this.Text = "Nova Viagem";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonNovoPacoteAplicar;
        private System.Windows.Forms.Button ButtonNovoPacoteCancelar;
        private System.Windows.Forms.TextBox TextBoxIdPacote;
        private System.Windows.Forms.RichTextBox TextBoxDescricaoPacote;
        private System.Windows.Forms.TextBox TextBoxPreco;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}