﻿namespace Pacotes
{
    using System.Collections.Generic;
    using System.Windows.Forms;
    using LivrariaDeClasses;

    public partial class FormApagarPacote : Form
    {
        List<Packs> ListaDePacotes = ListaDePacks.LoadPacotes();

        new Form1 Menu;
        public FormApagarPacote(Form1 menu)
        {
            InitializeComponent();
            this.Menu = menu;
            ComboBoxApagarPacotes.DataSource = ListaDePacotes;

        }

        private void ButtonCancelar_Click(object sender, System.EventArgs e)
        {
            Close();
            Menu.Show();

        }

        private void ButtonApagar_Click(object sender, System.EventArgs e)
        {
            ListaDePacotes.Remove((Packs)ComboBoxApagarPacotes.SelectedItem);
            MessageBox.Show("Viagem Apagada com sucesso");
            ComboBoxApagarPacotes.DataSource = null;
            ComboBoxApagarPacotes.DataSource = ListaDePacotes;


        }
    }
}
