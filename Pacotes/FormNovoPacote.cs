﻿namespace Pacotes
{
    using System.Windows.Forms;
    using System.Collections.Generic;
    using System;
    using LivrariaDeClasses;

    public partial class FormNovoPacote : Form
    {
        new Form1 Menu;
        List<Packs> ListaDePacotes = ListaDePacks.LoadPacotes();

        public FormNovoPacote(Form1 menu)
        {
            InitializeComponent();            
            this.Menu = menu;
            TextBoxIdPacote.Text = (ListaDePacks.GerarIdPacote()).ToString();

        }
        //private int GerarIdPacote()
        //{
        //    if (ListaDePacotes.Count == 0)
        //    {
        //        return Convert.ToInt32(TextBoxIdPacote.Text = "1"); 
        //    }
        //    else
        //    {
        //        return ListaDePacotes[ListaDePacotes.Count - 1].IdPacote + 1;
        //    }
        //}

        private void ButtonNovoPacoteAplicar_Click(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBoxDescricaoPacote.Text))
            {
                MessageBox.Show("Insira a Descrição da Viagem.");
                return;
            }
        
            double preco;

            if (!double.TryParse(TextBoxPreco.Text, out preco))
            {
                MessageBox.Show("Insira o Preço da Viagem");
                return;
            }
            
            

            var pacote = new Packs
            {
                IdPacote = ListaDePacks.GerarIdPacote(),
                DescricaoPacote = TextBoxDescricaoPacote.Text,
                Precopacote = preco
            };

            ListaDePacotes.Add(pacote);
            MessageBox.Show("Nova Viagem Inserida com Sucesso");
            Close();
            Menu.Show();
        }

        private void ButtonNovoPacoteCancelar_Click(object sender, EventArgs e)
        {
            Close();
            Menu.Show();
            
        }
    }
}
