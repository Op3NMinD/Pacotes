﻿namespace Pacotes
{
    using System.Windows.Forms;
    using System.Collections.Generic;
    using System;
    using Properties;
    using System.IO;
    using LivrariaDeClasses;

    public partial class Form1 : Form
    {
        List<Packs> ListaDePacotes = ListaDePacks.LoadPacotes();
        private FormNovoPacote FormularioNovoPacote;
        private FormApagarPacote FormularioApagarPacote;
        private FormEditarPacote FormularioEditarPacote;
        private FormListarPacotes FormularioListarPacote;


        public Form1()
        {
            InitializeComponent();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("v1.0.1 Criada por Rui Mendes");
        }

        private void PictureBoxAdicionar_Click(object sender, EventArgs e)
        {
            FormularioNovoPacote = new FormNovoPacote(this);
            Hide();
            FormularioNovoPacote.Show();

        }

        private void PictureBoxApagar_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                FormularioApagarPacote = new FormApagarPacote(this);
                Hide();
                FormularioApagarPacote.Show();
            }
            else
            {
                MessageBox.Show("Não existe Viagens para Apagar.");
            }
        }

        private void PictureBoxLer_Click(object sender, EventArgs e)
        {
            if (ListaDePacotes.Count != 0)
            {
                FormularioListarPacote = new FormListarPacotes(this);
                Hide();
                FormularioListarPacote.Show();
            }
            else
            {
                MessageBox.Show("Não existe Viagens para mostrar.");
            }

        }

        private void PictureBoxEdit_Click(object sender, EventArgs e)
        {
            try
            {
                FormularioEditarPacote = new FormEditarPacote(this);
                Hide();
                FormularioEditarPacote.Show();
            }
            catch
            {
                MessageBox.Show("Não existe viagens para editar.");

            }

        }

        private void PictureBoxFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBoxAdicionar_MouseHover(object sender, EventArgs e)
        {
            PictureBoxAdicionar.Image = Resources.add2;
        }

        private void PictureBoxAdicionar_MouseLeave(object sender, EventArgs e)
        {
            PictureBoxAdicionar.Image = Resources.add;
        }

        private void PictureBoxApagar_MouseHover(object sender, EventArgs e)
        {
            PictureBoxApagar.Image = Resources.Recycle2;
        }

        private void PictureBoxApagar_MouseLeave(object sender, EventArgs e)
        {
            PictureBoxApagar.Image = Resources.Recycle;
        }

        private void PictureBoxLer_MouseHover(object sender, EventArgs e)
        {
            PictureBoxLer.Image = Resources.zoom2;
        }

        private void PictureBoxLer_MouseLeave(object sender, EventArgs e)
        {
            PictureBoxLer.Image = Resources.zoom;
        }

        private void PictureBoxEdit_MouseHover(object sender, EventArgs e)
        {
            PictureBoxEdit.Image = Resources.refresh2;
        }

        private void PictureBoxEdit_MouseLeave(object sender, EventArgs e)
        {
            PictureBoxEdit.Image = Resources.refresh;
        }

        private void PictureBoxFechar_MouseHover(object sender, EventArgs e)
        {
            PictureBoxFechar.Image = Resources.close2;
        }

        private void PictureBoxFechar_MouseLeave(object sender, EventArgs e)
        {
            PictureBoxFechar.Image = Resources.close;
        }

        private void carregarListaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaDePacks.CarregarLista();
        }

        private void gravarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaDePacks.GravarLista();

        }

        private void gravarComoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaDePacks.GravarListaComo();

        }

    }
}
