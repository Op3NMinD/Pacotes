﻿namespace Pacotes
{
    partial class FormListarPacotes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridViewListar = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewListar)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridViewListar
            // 
            this.DataGridViewListar.AllowUserToAddRows = false;
            this.DataGridViewListar.AllowUserToDeleteRows = false;
            this.DataGridViewListar.AllowUserToResizeColumns = false;
            this.DataGridViewListar.AllowUserToResizeRows = false;
            this.DataGridViewListar.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridViewListar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.DataGridViewListar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DataGridViewListar.Location = new System.Drawing.Point(28, 12);
            this.DataGridViewListar.Name = "DataGridViewListar";
            this.DataGridViewListar.ReadOnly = true;
            this.DataGridViewListar.Size = new System.Drawing.Size(524, 185);
            this.DataGridViewListar.TabIndex = 0;
            this.DataGridViewListar.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DataGridViewListar_ColumnHeaderMouseClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Image = global::Pacotes.Properties.Resources.ic_cancel;
            this.button1.Location = new System.Drawing.Point(481, 214);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 34);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormListarPacotes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(585, 266);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.DataGridViewListar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormListarPacotes";
            this.Text = "Lista de Viagens";
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewListar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DataGridViewListar;
        private System.Windows.Forms.Button button1;
    }
}