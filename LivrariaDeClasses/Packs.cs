﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivrariaDeClasses
{
    public class Packs
    {
        public int IdPacote { get; set; }
        public string DescricaoPacote { get; set; }
        public double Precopacote { get; set; }


        //public string LinhaCompleta => $"Id da Viagem: {IdPacote} | Descrição da Viagem: {DescricaoPacote} | Preço: {Precopacote}€";

        public override string ToString()
        {
            return string.Format("Id da Viagem: {0}  |  Descrição da Viagem: {1}  |  Preço: {2}€", IdPacote, DescricaoPacote, Precopacote);
        }
    }
}
