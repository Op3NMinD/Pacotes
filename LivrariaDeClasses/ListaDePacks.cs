﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LivrariaDeClasses
{


    public class ListaDePacks
    {
        private static string localizarficheiro = null;
        private static List<Packs> ListaDePacotes = new List<Packs>();

        public static List<Packs> LoadPacotes()
        {
            //{
            //    ListaDePacotes.Add(new Packs { IdPacote = 1, DescricaoPacote = "Viagem à Jamaica", Precopacote = 400 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 2, DescricaoPacote = "Viagem à Tailândia", Precopacote = 300 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 3, DescricaoPacote = "Viagem à China", Precopacote = 500 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 4, DescricaoPacote = "Viagem a Itália", Precopacote = 400 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 5, DescricaoPacote = "Viagem ao Japão", Precopacote = 500 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 6, DescricaoPacote = "Viagem a França", Precopacote = 400 });
            //    ListaDePacotes.Add(new Packs { IdPacote = 7, DescricaoPacote = "Viagem a Madagascar", Precopacote = 200 });
            //}

            return ListaDePacotes;
        }

        public static int GerarIdPacote()
        {
            if (ListaDePacotes.Count == 0)
            {
                return 1;
            }
            else
            {
                return ListaDePacotes[ListaDePacotes.Count - 1].IdPacote + 1;
            }
           
        }

  

        public static void CarregarLista()
        {

            StreamReader myStream;
            OpenFileDialog ficheiro = new OpenFileDialog();


            //ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.Title = "Open an text File";
            ficheiro.RestoreDirectory = true;

            if (ficheiro.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = new StreamReader(ficheiro.OpenFile())) != null)
                    {
                        using (myStream)
                        {
                            ListaDePacotes.Clear();

                            string linha = "";

                            while ((linha = myStream.ReadLine()) != null)
                            {
                                string[] campos = new string[3];
                                campos = linha.Split(';');

                                var pacote = new Packs
                                {
                                    IdPacote = Convert.ToInt32(campos[0]),
                                    DescricaoPacote = campos[1],
                                    Precopacote = Convert.ToDouble(campos[2])
                                };

                                ListaDePacotes.Add(pacote);
                            }

                            myStream.Close();
                            localizarficheiro = ficheiro.FileName;

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        public static void GravarLista()
        {

            if (localizarficheiro != null)
            {
                StreamWriter sw = new StreamWriter(localizarficheiro);
                try
                {
                    if (!File.Exists(localizarficheiro))
                    {
                        sw = File.CreateText(localizarficheiro);
                    }

                    foreach (var pacote in ListaDePacotes)
                    {
                        string linha = string.Format("{0};{1};{2}", pacote.IdPacote, pacote.DescricaoPacote, pacote.Precopacote);
                        sw.WriteLine(linha);
                    }

                    sw.Close();
                }
                catch (Exception ev)
                {
                    MessageBox.Show(ev.Message);
                }
            }

            else
            {
                MessageBox.Show("Tem que gravar como.. primeiro");
            }
        }

        public static void GravarListaComo()
        {
            SaveFileDialog ficheiro = new SaveFileDialog();

            //ficheiro.InitialDirectory = "c:\\";
            ficheiro.Filter = "Text file|*.txt";
            ficheiro.Title = "Save an text File";
            ficheiro.ShowDialog();

            if (ficheiro.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                FileStream fs = (FileStream)ficheiro.OpenFile();

                StreamWriter sw = new StreamWriter(fs);//se nao existir, grava

                //verficar se existe algum problema


                foreach (var pacote in ListaDePacotes)
                {
                    string linha = string.Format("{0};{1};{2}", pacote.IdPacote, pacote.DescricaoPacote, pacote.Precopacote);
                    sw.WriteLine(linha);

                }
                sw.Close();


                fs.Close();
                //localizarficheiro = ficheiro.FileName;
            }

        }
    }
}
